# Author:		Dustin Haring
# Copyright: 	Copyright (C) 2019 Dustin Haring - All Rights Reserved.
# License:		MIT License
# Date Created:	April 30, 2019
# Resources:	http://www.gnu.org/software/make/manual/make.html 
#				http://nuclear.mutantstargoat.com/articles/make/#practical-makefile
# Desc:			This makefile is used to setup, build, link and test the bootloader and
#				 kernel using the GNU cross-compiler toolchain setup in ../Compilers

## Parameters ##
TARGET=i686-elf

CC=../Compilers/opt/cross/bin/$(TARGET)-gcc
#CXX=../Compilers/opt/cross/bin/$(TARGET)-g++
AS=../Compilers/opt/cross/bin/$(TARGET)-as
LD=../Compilers/opt/cross/bin/$(TARGET)-ld
##						##

C_SRC=$(wildcard Kernel/src/*.c)
OBJ_DIR=obj
KERNEL_INC_DIR=Kernel/include

BOOTLOADER_S_SRC=$(wildcard Bootloader/*.s)
KERNEL_S_SRC=$(wildcard Kernel/src/*.s)
BOOT_LINKER_FILE=Bootloader/linker.ld
KERNEL_LINKER_FILE=Kernel/linker.ld

# define any directories containing header files other than /usr/include
C_INC=-I$(KERNEL_INC_DIR)

# Set compiler flags 
#src: https://gcc.gnu.org/onlinedocs/gcc/Option-Summary.html
# Compiler_FLAGS:
#	-ffreestanding: Assert that compilation targets a freestanding environment. This implies -fno-builtin. A freestanding environment is one in which the standard library may not exist, and program startup may not necessarily be at main.
#	-O2:			Optimize even more. GCC performs nearly all supported optimizations that do not involve a space-speed tradeoff. As compared to -O, this option increases both compilation time and the performance of the generated code. 
#	-Wall			Enables additional compiler warnings
#	-Wextra			Further enables addionation compiler warnings not included by -Wall
#	-std=gnu99		Sets what language standard is used. gnu99: GNU dialect of ISO C99
#	-nostdlib: 		Standard system libraries are not used
#	-lgcc:			This library (libgcc.a) of internal subroutines is used by GCC to overcome shortcomings of particular machines or special needs for some languages. We include it because the -nostdlib flag causes this lib to be ommitted. Including it ensures that you have no unresolved references to internal GCC library subroutines.
CFLAGS= -ffreestanding -O2 -Wall -Wextra -std=gnu99 -nostdlib -lgcc


# Set linker flags 
#	--oformat:		Set output file format
#   -nostdlib: 		Standard system libraries are not used
LINKER_FLAGS= --oformat binary -nostdlib

# define library paths in addition to /usr/lib
#   if I wanted to include libraries not in /usr/lib I'd specify
#   their path using -Lpath, something like:
# LDFLAGS=



#target … : prerequisites …
#        recipe

# Where
#		target -> name of file generated
#		prerequisites ->  a file that is used as input to create the target
#		recipe -> actions taken by make. Tab is required.

.PHONY: build bootloader kernel linkers test test_bootloader test_kernel clean download dependencies install clean-all

build: bootloader kernel linkers
	echo "Finished!"

bootloader:
	# Check that obj directory exists, else create it
	mkdir -p obj 

	## Bootstrap Assembly: build the assembler bootloader and put object in objs directory
	$(AS) $(BOOTLOADER_S_SRC) -o obj/boot.o

kernel:
	##Compile kernel
	mkdir -p obj

	# Compile kernel assembler files. This is primarily for the kernel_loader and procted mode files
	$(AS) $(KERNEL_S_SRC) -o obj/kernel_assembler.o

	# Compile kernel C code:
	$(CC) -c $(C_SRC) $(C_INC) -o obj/kernel.o $(CFLAGS)

linkers: obj/boot.o $(KERNEL_LINKER_FILE) $(BOOTLOADER_LINKER_FILE) obj/kernel.o obj/kernel_assembler.o
	# Linking Bootloader files
	$(LD) -T $(BOOT_LINKER_FILE) obj/boot.o $(LINKER_FLAGS) -o bootloader.bin

	# Linking Kernel assembler and .c files
	$(LD) -T $(KERNEL_LINKER_FILE) obj/kernel_assembler.o obj/kernel.o $(LINKER_FLAGS) -o kernel.bin
	
	# Combine the binary files and save as the OS binary file
	cat bootloader.bin kernel.bin > ../HarOS.bin

verify_grub: kernel.bin
	if grub-file --is-x86-multiboot kernel.bin; then \
	echo "\n\nSuccess: Multiboot Confirmed";\
	else \
	echo "\n\nFailed: Multiboot Test Failed!";\
	fi

# TODO  Implement file system to create iso stuff
#iso: bootloader.bin kernel.bin
#	# A big shout out to http://mikeos.sourceforge.net/ for showing me how to create the
#	#  necessary .flp and .iso images from binary files when Google and Stackoverflow failed me!
#
#	# Ensure disk_images directory exists, create it otherwise
#	mkdir -p disk_images
#
#	# Create .flp image from the bootloader.bin (bootloader binary)
#	echo ">>> Adding bootloader to floppy image..."
#	dd status=noxfer conv=notrunc if=bootloader.bin of=disk_images/HaringBooloader.flp
#
#	# Cleanup
#	rm -rf tmp_loop
#
#	# Create temporary directory, and copy kernel.bin to mounted directory.
#	mkdir tmp_loop
#	
#	# Mount the HaringBootloader.flp floppy image to temp-loop directory
#	sudo mount -o loop -t vfat disk_images/HaringBootloader.flp tmp_loop 
#	
#	# Copy kernel.bin binary data to temp-loop directory that we mounted in previous step
#	echo ">>> Copying Haring Kernel and Programs..."
#	cp kernel.bin tmp_loop/
#	
#	# Copy any remaining 'program' binaries to temp-loop
#	# Future implementation TODO 
#	#cp programs/*.bin programs/*.bas programs/sample.pcx tmp_loop
#
#	# Pause a second to be sure data is written. REVIEW 
#	sleep 1
#
#	# Unmount the tmp_loop directory
#	echo ">>> Unmounting loopback floppy..."
#	umount tmp_loop
#
#	# Cleanup
#	rm -rf tmp_loop
#
#	# Create ISO image from .flp images
#	echo ">>> Creating CD-ROM ISO image..."
#
#	rm -f disk_images/HaringOS.iso
#	mkisofs -quiet -V 'HAROS' -input-charset iso8859-1 -o disk_images/HaringOS.iso -b HaringBootloader.flp disk_images/ || exit
#
#	echo '>>> Done!'
#
#	echo ">>> Creating new HarOS (HaringOS) floppy image..."
#	mkdosfs -C disk_images/HaringBootloader.flp 1440 || exit

test_kernel: kernel.bin
	qemu-system-i386 -kernel kernel.bin

test_bootloader: bootloader.bin
	qemu-system-i386 -fda bootloader.bin

test: ../HarOS.bin
	qemu-system-i386 -fda ../HarOS.bin

 clean :
	rm -rf ../HarOS.bin obj/* obj bootloader.bin kernel.bin

dependencies:
download:
install:
clean-all:
	make -s clean