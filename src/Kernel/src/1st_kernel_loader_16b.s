/*
* Author:		    Dustin Haring
* Copyright: 	  Copyright (C) 2019 Dustin Haring - All Rights Reserved.
* License:		  MIT License
* Date Created:	March 28, 2019
* Resources:	
*           https://theartofmachinery.com/2017/01/24/boot_to_d.html
*           http://3zanders.co.uk/2017/10/18/writing-a-bootloader3/
* Desc:			This AT&T syntax assembler code is called from the bootloader after being
*           loaded into ram. It prints to the screen and calls _enter_protected_mode in
*           enter_protected_mode.s to boot into protected mode and start the kernel.
*/

.code16                 # tell assembler to compile code for/as 16-bit

.section .kernel_loader # This is the name of the section the following code is part of. This is used when linking and sections are organized in the linker.ld file.

/* Variables */
 MSG_BOOT: .asciz "This is the HaringKernel Kernel Loader!\n\r"

.global _kernel_loader # Make _kernel_loader visible to linker
_kernel_loader:
  
  mov $MSG_BOOT, %si
  call print_string
  jmp _enter_protected_mode /* Enter entering protected mode, then start the kernel. These actions are located in the enter_protected_mode.s file */

finished: /* loop forever should the kernel ever exit */
  cli # clear interrupt flag
  hlt # Halt CPU execution
  jmp finished

/* Set the address to the string to print to the screen in si and call this label */
print_string:
  pusha                 # Pushes all the general purpose registers onto the stack in the following order: AX, CX, DX, BX, SP, BP, SI, DI. The value of SP pushed is the value before the instruction is executed. It is useful for saving state before an operation that could potentially change these registers. 

  print_string.loop:
    lodsb               # Loads single byte addressed by DS:SI into al and increments si (if DF=0, else if DF=1, decrements) to point to next char in char array.
    cmpb $0,%al         # Compares 0x0000 byte (char array null termination character) and the content of AL (the current char from char array)
    je print_string.ret # Jump-if-equal, if al == 0 (flag CF==0), then go to "done"...This checks if we have reached the end of our msg string.
    movb $0x0E,%ah      # Write Character in TTY Mode function for Interrupt 0x10
    int $0x10           # Raise Video Services Interrupt to tell BIOS to print the character in al to screen
    jmp print_string.loop # repeat process for next byte

  print_string.ret: # The code here resets the cursor to the 0th column of the next row and returns
    popa            # Pops all the general purpose registers off the stack in the reverse order of PUSHA. That is, DI, SI, BP, SP, BX, DX, CX, AX. Used to restore state after a call to PUSHA. 
    ret             # Return to location called from
