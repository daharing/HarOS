/*
* Author:		    Dustin Haring
* Copyright: 	  Copyright (C) 2019 Dustin Haring - All Rights Reserved.
* License:		  MIT License
* Date Created:	March 28, 2019
* Resources:
*           https://theartofmachinery.com/2017/01/24/boot_to_d.html
*           http://3zanders.co.uk/2017/10/18/writing-a-bootloader3/
*           https://blog.ghaiklor.com/how-to-implement-a-second-stage-boot-loader-80e75ae4270c
*           https://web.eecs.umich.edu/~prabal/teaching/resources/eecs373/Assembler.pdf
*           https://ftp.gnu.org/old-gnu/Manuals/gas-2.9.1/html_chapter/as_7.html GNU commands
*           https://www.codeproject.com/Articles/664165/Writing-a-boot-loader-in-Assembly-and-C-Part
*
* Desc:			This AT&T syntax assembler code is placed in the boot sector
*           and the last 2 bytes are marked (located in the linker.ld) as bootable
*           by setting these bytes to 0xaa55. Keep in mind that this is in endian 
*           format. This code starting at _bootloader_main is what the BIOS loads
*           into ram and executes (memory location 0x7c00). This will load the next
*           2nd stage bootloader called the kernel_loader located in the 
*           src/Kernel/src/1st_kernel_loader_16b.s file.
*/


.code16    # tell assembler to compile code for/as 16-bit
.text      # Tells assembler compiler that actual machine instructions are contained here

# .globl <label> # Makes <label> visible to linker. Essentially makes the lable globally visible, like public functions in C/C++.
.globl _bootloader_main;  # Make _bootloader_main visible to linker

.equ KERNEL_OFFSET, 0x1000  # .equ is equivalent to #define in c.
                            # where to load kernel's boot loader binaries to in memory relative to our location

_bootloader_main:  # Code execution begins here as defined in the linker.ld script # _main would be the 1st byte in the boot (1st) section
  cli                # Clear interrupts 
  
  movb %dl,BOOT_DRIVE          # Save the drive we booted from to the BOOT_DRIVE variable
  call load_kernel             # Load the kernel's bootloader from the storage device (HDD/SDD/FD....) to ram
  call execute_kernel  # Execute the kernel's bootloader to boot/execute the kernel
  jmp exit


execute_kernel:

  # Print message to screen
  movw $MSG_EXECUTING_BOOT, %si # Loads the address of MSG_EXECUTING_BOOT into si - source index register.
  call print_string     # Call print_string function to print string to screen

  call KERNEL_OFFSET    # Call/Execute the kernel_start which was read from disk starting at KERNEL_OFFSET. The code is found in kernel/src/kernel_start_32b.s

load_kernel: 
  pusha           # Pushes all the general purpose registers onto the stack in the following order: AX, CX, DX, BX, SP, BP, SI, DI. The value of SP pushed is the value before the instruction is executed. It is useful for saving state before an operation that could potentially change these registers. 
  
  # Print message to screen
  movw $MSG_LOADING_BOOT, %si # Loads the address of MSG_LOADING_BOOT into si - source index register.
  call print_string     # Call print_string function to print string to screen

  movw $KERNEL_OFFSET, %bx  # bx stores the pointer to the location data read from the disk should be stored
                            # In this case, the data read will be stored at the KERNEL_OFFSET location
  movb $15,%dh              # dh is the number of sectors to read from the disk
  movb BOOT_DRIVE,%dl       # dl is the drive number to read from
  call disk_read            # Call 'function' to read from disk

  # Data read is stored at pointer location of pointer in bx ( at KERNEL_OFFSET )

  popa                      # Pops all the general purpose registers off the stack in the reverse order of PUSHA. That is, DI, SI, BP, SP, BX, DX, CX, AX. Used to restore state after a call to PUSHA. 
  ret                       # Return to location called from

disk_read: # Read from memory (like HDD/SSD/FD/CD/DVD memory)
              # The drive number to read from is stored in the dl register and should be set before calling this 'function'
              # ES:BX is the pointer to the buffer where read data will be store. Must be set before calling this 'function'
              # dh is numer of sectors to read
  pusha           # Pushes all the general purpose registers onto the stack in the following order: AX, CX, DX, BX, SP, BP, SI, DI. The value of SP pushed is the value before the instruction is executed. It is useful for saving state before an operation that could potentially change these registers. 
  pushw %dx       # Push word ...Why do we need this? REVIEW 

  # ## SEE THE Read Disk Sectors Section Below in Documention area
  #	AH = 02
  #	AL = number of sectors to read	(1-128 dec.)
  #	CH = track/cylinder number  (0-1023 dec., see below)
  #	CL = sector number  (1-17 dec.)
  #	DH = head number  (0-15 dec.)
  #	DL = drive number (0=A:, 1=2nd floppy, 80h=drive 0, 81h=drive 1)
  #	ES:BX = pointer to buffer
  #
  #	On return:
  #	AH = status  (see INT 13,STATUS)
  #	AL = number of sectors read
  #	CF = 0 if successful
  #	   = 1 if error
  movb $0x02,%ah  # Read Sector function for interrupt 0x13 (Disk Services)
  movb %dh,%al    # Set al to the number of sectors to read for comparison later
  movb $0x00,%ch  # Set to read from track 0
  movb $0x00,%dh  # Set to read from head 0
  movb $0x02,%cl  # Set to read from sector 2
  int $0x13       # Raise Disk Service interrupt ( ah == 0x02 -> read sectors)

  jc memory_read_error  # Jump if carry flag set (CF==1) 

  popw %dx        # Pop word ...Why do we need this? REVIEW 
  cmpb %al,%dh    # Compare bytes/registers... Checks if number of sectors read from disk equals the number of sectors we wanted to read
  jne memory_read_error # Jump if flag says register AL != DH ( flag ZF=0 )

  popa  # Pops all the general purpose registers off the stack in the reverse order of PUSHA. That is, DI, SI, BP, SP, BX, DX, CX, AX. Used to restore state after a call to PUSHA. 
  ret   # Return to location called from

memory_read_error: 
  mov $MSG_MEMORY_READ_ERROR, %si # Loads the address of boot_msg into si - source index register.
  call print_string # Call print_string function to print string to screen
  hlt               # CPU command to stop execution



print_string:
  pusha      # Pushes all the general purpose registers onto the stack in the following order: AX, CX, DX, BX, SP, BP, SI, DI. The value of SP pushed is the value before the instruction is executed. It is useful for saving state before an operation that could potentially change these registers. 

  print_string.loop: 
    lodsb               # Loads single byte addressed by DS:SI into al and increments si (if DF=0, else if DF=1, decrements) to point to next char in char array.
    cmpb $0,%al         # Compares 0x0000 byte (char array null termination character) and the content of AL (the current char from char array)
    je print_string.ret # Jump-if-equal, if al == 0 (flag CF==0), then go to "done"...This checks if we have reached the end of our msg string.
    movb $0x0E,%ah      # Write Character in TTY Mode function for Interrupt 0x10
    int $0x10           # Raise Video Services Interrupt to tell BIOS to print the character in al to screen
    jmp print_string.loop # repeat process for next byte

  print_string.ret:
  
  popa  # Pops all the general purpose registers off the stack in the reverse order of PUSHA. That is, DI, SI, BP, SP, BX, DX, CX, AX. Used to restore state after a call to PUSHA. 
  ret   # Return to location called from


exit:
  cli # clear interrupt flag
  hlt # CPU command to stop/halt execution
  jmp exit

/* Variables */
  BOOT_DRIVE: .byte 0   # Variable to store the drive number that we booted from.
  
  # '\n' sets the cursor to the next line on the screen
  # '\r' resets the cursor to column 0 on the screen.
  MSG_LOADING_BOOT: .asciz "Loading Kernel_Start from Disk...\n\r"
  MSG_EXECUTING_BOOT: .asciz "Execute Kernel_Start...\n\r"
  MSG_MEMORY_READ_ERROR: .asciz "Memory Storage Device Error!\n\r Failed to load kernel from disk\n\r"




# NOTES #
#
# https://en.wikibooks.org/wiki/X86_Assembly/Control_Flow
#
# VERY GOOD AND LOTS OF INFORMATION. ABS NECESSARY!
# https://gist.github.com/mishurov/6bcf04df329973c15044
#
# BIOS Interrupts: https://en.wikipedia.org/wiki/BIOS_interrupt_call
#
# General purpose registers: These are used to store temporary data required by the program during its lifecycle. Each of these registers is 16 bit wide or 2 bytes long.
# 
#     AX - the accumulator register
#     BX - the base address register
#     CX - the count register
#     DX - the data register
# 
# Segment Registers: To represent a memory address to a microprocessor, there are two terms we need to be aware of:
# 
#     Segment: It is usually the beginning of the block of a memory.
#     Offset: It is the index of memory block onto it.
# 
# Example: Suppose say, there is a byte whose value is 'X' that is present on a block of memory whose start address is 0x7c00 and the byte is located at the 10th position from the beginning. In this situation, We represent segment as 0x7c00 and the offset as 10.
# The absolute address is 0x7c00 + 10.
# 
# There are four categories that I wanted to list out.
# 
#     CS - code segment
#     SS - stack segment
#     DS - data segment
#     ES - extended segment
# 
# Stack Registers:
# 
#     BP - base pointer
#     SP - stack pointer
# 
# Index Registers:
# 
#     SI - source index register.
#     DI - destination index register.
#     AX: CPU uses it for arithmetic operations.
#     BX: It can hold the address of a procedure or variable (SI, DI, and BP can also). And also perform arithmetic and data movement.
#     CX: It acts as a counter for repeating or looping instructions.
#     DX: It holds the high 16 bits of the product in multiply (also handles divide operations).
#     CS: It holds base location for all executable instructions in a program.
#     SS: It holds the base location of the stack.
#     DS: It holds the default base location for variables.
#     ES: It holds additional base location for memory variables.
#     BP: It contains an assumed offset from the SS register. Often used by a subroutine to locate variables that were passed on the stack by a calling program.
#     SP: Contains the offset of the top of the stack.
#     SI: Used in string movement instructions. The source string is pointed to by the SI register.
#     DI: Acts as the destination for string movement instructions.
# 
# The registers are further divided as below following left to right order or bits:
#      AX: The first 8 bits of AX is identified as AL and the last 8 bits is identified as AH
#      BX: The first 8 bits of BX is identified as BL and the last 8 bits is identified as BH
#      CX: The first 8 bits of CX is identified as CL and the last 8 bits is identified as CH
#      DX: The first 8 bits of DX is identified as DL and the last 8 bits is identified as DH
#
#
# EAX,EBX,ECX,EDX - "general purpose", more or less interchangeable
#
#  EBP             - used to access data on stack
#                  - when this register is used to specify an address, SS is
#                    used implicitly
#
#  ESI,EDI         - index registers, relative to DS,ES respectively
#
#  SS,DS,CS,ES,FS,GS - segment registers
#                    - (when Intel went from the 286 to the 386, they figured
#                        that providing more segment registers would be more
#                        useful to programmers than providing more general-
#                        purpose registers... now, they have an essentially
#                        RISC processor with only _FOUR_ GPRs!)
#                    - these are all only 16 bits in size
#
#  EIP            - program counter (instruction pointer), relative to CS
#
#  ESP            - stack pointer, relative to SS
#
#  EFLAGS         - condition codes, a.k.a. flags
#
# 
# 
# <reg32> Any 32-bit register (%eax, %ebx, %ecx, %edx, %esi, %edi, %esp, or %ebp)
# <reg16> Any 16-bit register (%ax, %bx, %cx, or %dx)
# <reg8> 	Any 8-bit register (%ah, %bh, %ch, %dh, %al, %bl, %cl, or %dl)
# <reg> 	Any register
# <mem> 	A memory address (e.g., (%eax), 4+var(,1), or (%eax,%ebx,1))
# <con32> Any 32-bit immediate
# <con16> Any 16-bit immediate
# <con8> 	Any 8-bit immediate
# <con> 	Any 8-, 16-, or 32-bit immediate
#   
#
#      Flags:
#    SF ≔ MostSignificantBit(difference), so a unset SF means the difference is non-negative (minuend ≥ subtrahend [NB: signed comparison])
#    ZF ≔ (difference = 0)
#    PF ≔ BitWiseXorNor(difference[Max-1:0])
#    CF, OF and AF
# 
# ascii data type represents a group of bytes without a null terminator.
# asciz type to represents a group of bytes terminated with a null character at the end.
#
# Templates:
#
#    ## Print Letter to screen in real mode
#        #print letter 'H' onto the screen
#         movb $'H' , %al     # Move literal 'H' into the lower (first) nibble of register A.
#         movb $0x0e, %ah     # Move literal 0x0e into the upper (second) nibble of register A. 
#         int  $0x10


# movb $2, (%si)    /* Move 2 into the single byte at the address stored in EBX. */
# movw $2, (%ebx) 	/* Move the word (16-bit) integer representation of 2 into the 2 bytes starting at the address in EBX. */
# movl $2, (%ebx)   /* Move the long (32-bit) integer representation of 2 into the 4 bytes starting at the address in EBX. */ 
# mov %ebx, %eax — copy the value in EBX into EAX
#
# 
# ############ Reading from the Disk ############
# INT 13,2 - Read Disk Sectors
#
#	AH = 02
#	AL = number of sectors to read	(1-128 dec.)
#	CH = track/cylinder number  (0-1023 dec., see below)
#	CL = sector number  (1-17 dec.)
#	DH = head number  (0-15 dec.)
#	DL = drive number (0=A:, 1=2nd floppy, 80h=drive 0, 81h=drive 1)
#	ES:BX = pointer to buffer
#
#	on return:
#	AH = status  (see INT 13,STATUS)
#	AL = number of sectors read
#	CF = 0 if successful
#	   = 1 if error
#
#	- BIOS disk reads should be retried at least three times and the
#	  controller should be reset upon error detection
#	- be sure ES:BX does not cross a 64K segment boundary or a
#	  DMA boundary error will occur
#	- many programming references list only floppy disk register values
#	- only the disk number is checked for validity
#	- the parameters in CX change depending on the number of cylinders;
#	  the track/cylinder number is a 10 bit value taken from the 2 high
#	  order bits of CL and the 8 bits in CH (low order 8 bits of track):
#
#	  |F|E|D|C|B|A|9|8|7|6|5-0|  CX
#	   | | | | | | | | | |	`-----	sector number
#	   | | | | | | | | `---------  high order 2 bits of track/cylinder
#	   `------------------------  low order 8 bits of track/cyl number
#
# 


# 
# C escape sequences:
#
# Escape sequence 	Hex value in ASCII 	Character represented
# \a 	               07 	                Alert (Beep, Bell) (added in C89)[1]
# \b 	               08 	                Backspace
# \e	               1B 	                escape character
# \f 	               0C 	                Formfeed Page Break
# \n 	               0A 	                Newline (Line Feed); see notes below
# \r 	               0D 	                Carriage Return
# \t 	               09 	                Horizontal Tab
# \v 	               0B 	                Vertical Tab
# \\ 	               5C 	                Backslash
# \' 	               27 	                Apostrophe or single quotation mark
# \" 	               22 	                Double quotation mark
# \? 	               3F 	                Question mark (used to avoid trigraphs)
# \nnn 	             any 	                The byte whose numerical value is given by nnn interpreted as an octal number
# \xhh… 	           any 	                The byte whose numerical value is given by hh… interpreted as a hexadecimal number
# \uhhhh 	           none                 Unicode code point below 10000 hexadecimal
# \Uhhhhhhhh 	       none                 Unicode code point where h is a hexadecimal digit 





