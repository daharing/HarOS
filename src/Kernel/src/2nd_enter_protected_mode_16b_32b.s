/* 
* Author:     Dustin Haring
* Date:       April 30, 2019
* Last Modify:April 30, 2019
* Comment:
* Resource: 
*           http://3zanders.co.uk/2017/10/16/writing-a-bootloader2/
*           https://theartofmachinery.com/2017/01/24/boot_to_d.html
*           http://3zanders.co.uk/2017/10/18/writing-a-bootloader3/
* Description: The AT&T syntax code in this file is loaded from the hard drive
*             from the bootloader_16b_i686.s file (the bootloader). The purpose
*             of this code is to take the bootloader from real mode on the processor
*             and enter/setup protected mode, and then read the kernel_loader from
*             the hard drive. The kernel_loader is then executed.
*/

# ############################### PROTECTED MODE ###############################

.code16 /* Compile code below in 16-bit */

/* See the ## Setup Descriptor Table section */

.section .GlobalDescriptorTable
/* This table must be set between the enter_protected_mode section and the protected_mode section in the linker.
This is because of compilation problems with GAS. the code in this section much be placed above the 
enter_protected_mode section, or more specifically, above the jmp CODE_SEG:boot_protected_32b line. */
gdt_start:  /* Begining of Global Descriptor Table */

gdt_null:  # Global Descriptor Table NULL entry
  .quad 0x0         # null descriptor; ( .quad -> 8 bytes (quad -> 4 x words (16 bits) ). quad is 4 words or 64-bits )
gdt_code:   # Global Descriptor Table: code segment entry
  .word 0xFFFF      # limit_low
  .word 0x0         # base_low
  .byte 0x0         # base_middle
  .byte 0b10011010  # access
  .byte 0b11001111  # granularity
  .byte 0x0         # base_high
gdt_data:   # Global Descriptor Table: data segment entry
  .word 0xFFFF      # limit_low
  .word 0x0         # base_low
  .byte 0x0         # base_middle
  .byte 0b10010010  # access
  .byte 0b11001111  # granularity
  .byte 0x0         # base_high
gdt_end: /* end of Global Descriptor Table */

# To actually load this we also need a gdt pointer structure. This is a 16 bit 
#   field containing the GDT size followed by a 32 bit pointer to the structure itself.
gdt_pointer: 
    .word gdt_end - gdt_start       # Size of our Global Descriptor Table entries
    .long gdt_start                 # Adderss of the begining of our Global Descriptor Table entries

    .equ CODE_SEG, gdt_code - gdt_start   # #define CODE_SEG (size of the GDT code segment)   .equ is equivalent to a #define
    .equ DATA_SEG, gdt_data - gdt_start   # #define DATA_SEG (size of the GDT data segment) .equ is equivalent to a #define










# ############################### Entery Point ###############################

# ## Call _enter_protected_mode to enter and setup protected mode

.section .enter_protected_mode

.globl _enter_protected_mode /* .globl makes this label global to linker. */

_enter_protected_mode:  /* Call this label to enter protected mode */
    movw $0x2401,%ax  # enable the A20 line by calling the ‘A20-Gate activate’ function
    int $0x15         # Interrupt for the A20 for the 0x2401 A20 function
    movw $0x3,%ax     # Function to set video mode 3
    int $0x10         # Interrupt for function set vga text mode 3
    cli               # Clear interrupts

    /* Set up a Global Descriptor Table which will define a 32 bit code segment, 
    load it with the lgdt instruction, and then do a long jump to that code segment. */
    lgdt gdt_pointer    # Load descriptor table
    movl %cr0, %eax     # Get current CPU special functions register CR0
    orl $0x01, %eax     # Set the 1st bit of the CPU special register CR0 true to enter protected mode
    movl %eax, %cr0     # Set the CPU special regester CR0 with the new configuration (new config has protected mode bit set to enable protected mode)
    jmp $CODE_SEG, $boot_protected_32b # Long jump to the 32-bit code segment. A long jump is required to set CS with Code selector. We have now that we have entered protected mode










# ############################### Protected Mode 32-Bit Code ###############################
# ## Compile code below for 32-bit. This is the code that will run once the system has been booted into protected mode
.code32 
.section .protected_mode

/* Variables */
MSG_PROTECTED: .asciz "Hello from Protected Mode!"

boot_protected_32b:

  # Setup the segments
  movw $DATA_SEG, %ax   # Set the all the segments to point to the data segment
  movw %ax,%ds          # Set the all the segments to point to the data segment
  movw %ax,%es          # Set the all the segments to point to the data segment
  movw %ax,%fs          # Set the all the segments to point to the data segment
  movw %ax,%gs          # Set the all the segments to point to the data segment
  movw %ax,%ss          # Set the all the segments to point to the data segment


  # Print the message signifiying we are in protected mode
  movl $MSG_PROTECTED, %esi             # Load address of first char into si register
  call boot_protected_32b.print_string  # Call the print_string function after loading a string into %esi

  jmp _kernel_start # Go to code to start the kernel and setup the stack.

  jmp boot_protected_32b.exit           # jump to exit

boot_protected_32b.print_string:
  # https://en.wikipedia.org/wiki/VGA-compatible_text_mode
  # See the "Writing to VGA/Screen in Protected Mode" section
  pusha                     # Pushes all the general purpose registers onto the stack in the following order: AX, CX, DX, BX, SP, BP, SI, DI. The value of SP pushed is the value before the instruction is executed. It is useful for saving state before an operation that could potentially change these registers. 
  movl $0xb8000,%ebx        # Load the value 0xb8000 into the bx register; This is the address of the beginning of the physical video memory
  
  boot_protected_32b.print_string.loop:  
    lodsb             # Loads single byte addressed by DS:SI into al and increments si (if DF=0, else if DF=1, decrements) to point to next char in char array.
    cmpb $0,%al       # Compares 0x0000 byte (char array null termination character) and the content of AL (the current char from char array)
    je boot_protected_32b.print_string.return # Jump-if-equal, if al == 0 (flag CF==0), then go to "done"...This checks if we have reached the end of our msg string.
    orl $0x0F00,%eax  # logical or on a long; This is setting the 3rd bit true. Refering to the VGA Text Buffer table, you can see that this sets the screen background color. Color codes: https://en.wikipedia.org/wiki/Video_Graphics_Array#Color_palette
    movw %ax,(%ebx)   # Move the word (16-bit) into the address pointed to in bx, which is video memory
    addl $2,%ebx      # Move the address pointed to by bx forward by 2 bytes (16-bit) to the next screen char
    jmp boot_protected_32b.print_string.loop  # Loop until no more chars to print from string in esi

  boot_protected_32b.print_string.return:
    popa  # Pops all the general purpose registers off the stack in the reverse order of PUSHA. That is, DI, SI, BP, SP, BX, DX, CX, AX. Used to restore state after a call to PUSHA. 
    ret   # Return to location called from

start_kernel:

  /* jump to the kernel_start.s file. */
  jmp kernel_main /* Start Kernel */

  boot_protected_32b.exit: 
    cli # Clear interrupts
    hlt # Halt CPU
    jmp boot_protected_32b.exit # Infinite loop to prevent code from running off


# ##############################################################







# ############################### NOTES ###############################

# ## Setup Descriptor Table

# https://ozh.github.io/ascii-tables/
/*.----------------------.------.------.-----------.--------.-------.-----------.
  |     0                |   8  |  12  | 16        | 20     | 24    |    28     | 32-bit
  :----------------------+------+------+-----------+--------+-------+-----------:
  | limit_low            |  base_low   |base_middle| access | flags | base_high |
  '----------------------'-------------'-----------'--------'-------'-----------'
*/

/*  Access Layout
.---------.-----------.----------.-----------.------------.-----------.------------.----------.
|    0    |     1     |    2     |     3     |     4      |     5     |     6      |    7     | 8-bit
:---------+-----------+----------+-----------+------------+-----------+------------+----------:
| present |       ring_level     |     1     | executable | direction | read/write | accessed |
'---------'----------------------'-----------'------------'-----------'------------'----------'
*/

/* Flags Layout
.-------------.--------.-----------.-----------.------------.----------.----------.----------.
|      0      |   1    |     2     |     3     |     4      |    5     |    6     |    7     |  8-bit
:-------------+--------+-----------+-----------+------------+----------+----------+----------:
| granularity |  size  |     0     |     0     | limit_high                                  |
'-------------'--------'-----------'-----------'---------------------------------------------'
*/

# Here’s what the fields mean:
#
#    base a 32 bit value describing where the segment begins
#    limit a 20 bit value describing where the segment ends, can be multiplied by 4096 if granularity = 1
#    present must be 1 for the entry to be valid
#    ring level an int between 0-3 indicating the kernel Ring Level
#    direction
#        0 = segment grows up from base, 1 = segment grows down for a data segment
#        0 = can only execute from ring level, 1 = prevent jumping to higher ring levels
#    read/write if you can read/write to this segment
#    accessed if the CPU has accessed this segment
#    granularity 0 = limit is in 1 byte blocks, 1 = limit is multiples of 4KB blocks
#    size 0 = 16 bit mode, 1 = 32 bit protected mode



# ############ Writing to VGA/Screen in Protected Mode ############
/*
https://en.wikipedia.org/wiki/VGA-compatible_text_mode
The above link pretty well describes everything needed to know including the physical memory location
  of the text buffer to write to. The table below represents the 2-byte text buffer and how to set the
  colors and character to be displayed.

VGA has a fixed amount of memory and addresssing is 0xA0000 to 0xBFFFF.
0xA0000 for EGA/VGA graphics modes (64 KB)
0xB0000 for monochrome text mode (32 KB)
0xB8000 for color text mode and CGA-compatible graphics modes (32 KB)


For color codes, see https://en.wikipedia.org/wiki/Video_Graphics_Array#Color_palette
.------------------.------------------.-----------------.----------------.
| 0                | 4                | 8               |              15| 16-bit
:------------------+------------------+----------------------------------:
| Background_Color | Foreground_Color | ASCII Character                  |
'------------------'------------------'----------------------------------'
*/