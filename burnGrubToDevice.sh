#!/usr/bin/env bash 

## Note, if dd is failing for some reason, be sure that HarOS.iso exists, and that the /dev/sdx device exists and is operational

## https://wiki.osdev.org/Bare_Bones section "Booting the Kernel"

#http://tldp.org/LDP/Bash-Beginners-Guide/html/sect_08_02.html

PATH=$1

while [ -z "$PATH" ]; do
    echo "This script requires the path to the device (i.e. /dev/sdx )!"
    echo -n "Please enter the path to the device you wish to load the program to in the /dev/sdx format: "
    read -r PATH
done

echo -n "DANGER: Are you sure you wish to overwrite $PATH ? These actions cannot be undone [y/n]: "
read -n 1 RESPONSE 
echo

if [ "y" == "$RESPONSE" ]; then
    sudo dd if=HarOS.bin of="$PATH" && sync
    echo "Finished!"
fi
